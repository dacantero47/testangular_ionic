// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebase = {
  apiKey: "AIzaSyC4cpPwGuTINEUV4oJdkDaJxs9V8YLrsmM",
  authDomain: "prueba-36d85.firebaseapp.com",
  databaseURL: "https://prueba-36d85.firebaseio.com",
  projectId: "prueba-36d85",
  storageBucket: "",
  messagingSenderId: "605824180669",
  appId: "1:605824180669:web:96e7128911d6991f10f2d4"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
