import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private fireAuth: AngularFireAuth) { }
  login(email: string, password: string){
    this.fireAuth.auth.signInWithEmailAndPassword(email, password).then( rest => {
      console.log("Has sido logueado"+rest);
    })
    .catch(error => console.log("Ha ocurrido un error"+error));

  }
}
